# Daily Updates

### Day-2

- tried to understand how haproxy works by installing and playing around.
- went through these blogs to understand how current scenario is working with frontend, backend, haproxy.cfg, dockerfile, docker-compose file, how the workflow goes among all of these.
    - [https://www.cignex.com/blog/haproxy-load-balancer-installation-and-configuration-business-critical-applications](https://www.cignex.com/blog/haproxy-load-balancer-installation-and-configuration-business-critical-applications)
    - [https://infohub.delltechnologies.com/l/ecs-with-haproxy-load-balancer-2/haproxy-configuration-for-single-setup](https://infohub.delltechnologies.com/l/ecs-with-haproxy-load-balancer-2/haproxy-configuration-for-single-setup)
    - [https://support.ptc.com/help/thingworx/platform/r9/en/index.html#page/ThingWorx/Help/ThingWorxHighAvailability/HAProxyExample.html](https://support.ptc.com/help/thingworx/platform/r9/en/index.html#page/ThingWorx/Help/ThingWorxHighAvailability/HAProxyExample.html)
    - [https://docs.geoserver.geo-solutions.it/edu/en/clustering/load_balancing/haproxy.html](https://docs.geoserver.geo-solutions.it/edu/en/clustering/load_balancing/haproxy.html)
    - [https://www.redhat.com/sysadmin/configure-apache-haproxy-load-balance](https://www.redhat.com/sysadmin/configure-apache-haproxy-load-balance)
    - [https://www.digitalocean.com/community/tutorials/how-to-host-multiple-web-sites-with-nginx-and-haproxy-using-lxd-on-ubuntu-16-04](https://www.digitalocean.com/community/tutorials/how-to-host-multiple-web-sites-with-nginx-and-haproxy-using-lxd-on-ubuntu-16-04)
    - [https://www.digitalocean.com/community/tutorials/how-to-use-haproxy-to-set-up-http-load-balancing-on-an-ubuntu-vps](https://www.digitalocean.com/community/tutorials/how-to-use-haproxy-to-set-up-http-load-balancing-on-an-ubuntu-vps)
    - [https://www.digitalocean.com/community/tutorials/how-to-set-up-laravel-nginx-and-mysql-with-docker-compose-on-ubuntu-20-04](https://www.digitalocean.com/community/tutorials/how-to-set-up-laravel-nginx-and-mysql-with-docker-compose-on-ubuntu-20-04)
    - [https://heavyhorst.github.io/remco/examples/haproxy/](https://heavyhorst.github.io/remco/examples/haproxy/)
    - [https://www.bluematador.com/blog/running-haproxy-docker-containers-kubernetes](https://www.bluematador.com/blog/running-haproxy-docker-containers-kubernetes)
- made few changes in the code, was getting this error for long
```
itpod /workspace/gp_sandbox/DevOpsTest/DevOpsTest (main) $ docker-compose up -d
[+] Running 3/4
 ⠿ Container devopstest-staging-db_cms        Started                                                      0.4s
 ⠿ Container devopstest-preview-php_cms       Started                                                      0.9s
 ⠿ Container devopstest-preview-webserver     Started                                                      1.3s
 ⠏ Container devopstest-preview-loadbalancer  Starting                                                     0.9s
Error response from daemon: failed to create shim task: OCI runtime create failed: runc create failed: unable to start container process: exec: "/usr/local/bin/entrypoint.sh": permission denied: unknown
gitpod /workspace/gp_sandbox/DevOpsTest/DevOpsTest (master) $ 
```
- poked around for long, still couldn't find a solution to this error.
- by far no improvemment today.

### Day 3

- All the containers working, the log files are at this [place](Logs/Day3-25-08/output1.txt)
- Couldn't figure what does tagging the stage means
    - maybe it's tagging individually every image
    - maybe it's new way of tagging all image at once using docker-compose
    - or whole docker-compose maybe creating something in behind which represents all images, I need to tag that result.
- Looked into these sites to understand to do this
    - [https://stackoverflow.com/questions/33816456/how-to-tag-docker-image-with-docker-compose](https://stackoverflow.com/questions/33816456/how-to-tag-docker-image-with-docker-compose)
    - [https://stackoverflow.com/questions/53416685/docker-compose-tagging-and-pushing](https://stackoverflow.com/questions/53416685/docker-compose-tagging-and-pushing)
    - [https://docs.docker.com/engine/reference/commandline/tag/](https://docs.docker.com/engine/reference/commandline/tag/)
    - [https://stackoverflow.com/questions/33816456/how-to-tag-docker-image-with-docker-compose](https://stackoverflow.com/questions/33816456/how-to-tag-docker-image-with-docker-compose)
    - [https://docs.docker.com/engine/reference/commandline/image_inspect/](https://docs.docker.com/engine/reference/commandline/image_inspect/)


### Day 4

> Phase-1 is done

- Removed the tags I added last time to the individual images and the tags I added can be seen [here](https://gitlab.com/measutosh/devopstest/-/tags)

> Phase-2 is done

- Steps followed
    - Updated to tags to latest
    -   ```
        docker compose stop
        docker-compose down --rmi all
        docker-compose pull
        docker-compose up -d
        ```
- Used the following articles 
    - [https://stackoverflow.com/questions/18216991/create-a-tag-in-a-github-repository](https://stackoverflow.com/questions/18216991/create-a-tag-in-a-github-repository)
    - [https://stackoverflow.com/questions/49316462/how-to-update-existing-images-with-docker-compose](https://stackoverflow.com/questions/49316462/how-to-update-existing-images-with-docker-compose)
    - [https://stackoverflow.com/questions/49316462/how-to-update-existing-images-with-docker-compose#:~:text=Usage%3A%20compose%2Dupdate%20%5BOPTIONS,yaml%22%2C%20%22compose.](https://stackoverflow.com/questions/49316462/how-to-update-existing-images-with-docker-compose#:~:text=Usage%3A%20compose%2Dupdate%20%5BOPTIONS,yaml%22%2C%20%22compose.)
    - [https://stackoverflow.com/questions/37685581/how-to-get-docker-compose-to-use-the-latest-image-from-repository](https://stackoverflow.com/questions/37685581/how-to-get-docker-compose-to-use-the-latest-image-from-repository)
    - [https://www.baeldung.com/ops/docker-compose-latest-image](https://www.baeldung.com/ops/docker-compose-latest-image)

> Phase-3 in progress

- Haven't done any backups by myself, trying to figure out how it works, went through few resources. Yet to find out how to execute the task in current scenario.
- Went through these :
    - [https://stackoverflow.com/questions/19664893/linux-shell-script-for-database-backup](https://stackoverflow.com/questions/19664893/linux-shell-script-for-database-backup)
    - [https://devanswers.co/backup-mysql-databases-linux-command-line/](https://devanswers.co/backup-mysql-databases-linux-command-line/)
    - [http://download.nust.na/pub6/mysql/doc/refman/5.1/en/mysqldump.html](http://download.nust.na/pub6/mysql/doc/refman/5.1/en/mysqldump.html)

**Major pending stuff till now** : I am still unable to see the site, I am using Gitpod, following the correct procedures, no idea why I am not able see the site, even the ports are working fine.

